# Obtenir les consonnes d'un message

## test 1
### Input
50% azucar, 50% miel

### Output
zcrml


## test 2
### Input
Un primo que viajó a Estados Unidos me dijo manda plata.

### Output
nprmqvjstdsndsmdjmndplt

## test 3
### Input
Te hubiera dicho que andabas de humorista

### Output
Thbrdchqndbsdhmrst
