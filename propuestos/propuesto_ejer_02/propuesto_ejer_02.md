# Additionner les chiffres d'une plage de nombres
Dado un límite inferior `xl` y un límite superior `xh`, halle la suma de los dígitos de `x` para cada `x` en el rango, incluyendo `xh`.

## test 1
### Input
```python
xl = 4
xh = 50
```

### Output
`[4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 5]`


## test 2
### Input
```python
xl = 600
xh = 632
```

### Output
`[6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 9, 10, 11]`

## test 3
### Input
```python
xl = 0
xh = 42
```

### Output
`[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 4, 5, 6]`
